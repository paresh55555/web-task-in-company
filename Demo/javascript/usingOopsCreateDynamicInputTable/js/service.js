var ServiceApi = (function () {

    function ServiceApi() {
        this.arrDB = [];
        this.storageData = new StorageData(this.arrDB);
    }

    ServiceApi.prototype.getData = function () {
        return this.storageData.fnGetDB() ? this.storageData.fnGetDB() : this.arrDB;
    };

    ServiceApi.prototype.addApi = function (obj) {
        var arrDB = this.storageData.fnGetDB() ? this.storageData.fnGetDB() : this.arrDB;
        arrDB.push(obj);
        this.storageData.fnSetDB(arrDB);
    };

    ServiceApi.prototype.edtRowField = function (indexId,editVal) {
        var arrDB = this.storageData.fnGetDB() ? this.storageData.fnGetDB() : this.arrDB;
        var intIndex2 = arrDB.map(function (obj) {
            return obj.id;
        }).indexOf(parseInt(indexId));
       /* console.log('intIndex2',intIndex2);*/
        if (intIndex2 > -1) {
            var obj = arrDB[intIndex2];
            obj.isEdit = editVal;
            arrDB[intIndex2] = obj;
            this.storageData.fnSetDB(arrDB);
        }

       /* var arrDB = this.storageData.fnGetDB() ? this.storageData.fnGetDB() : this.arrDB;
        arrDB[parseInt(indexId)].isEdit = true;
        this.storageData.fnSetDB(arrDB);*/
    };

    ServiceApi.prototype.updRowField = function (updId, val, isEdit) {
        var arrDB = this.storageData.fnGetDB() ? this.storageData.fnGetDB() : this.arrDB;
       /* console.log('upddd..',arrDB);*/
        var intIndex = arrDB.map(function (obj) {
            return obj.id;
        }).indexOf(parseInt(updId));
        if (intIndex > -1) {
            var obj = arrDB[intIndex];
            obj.value = val;
            obj.isEdit = isEdit;
            arrDB[intIndex] = obj;
            this.storageData.fnSetDB(arrDB);
        }
    };

    ServiceApi.prototype.delRowField = function (delId) {
        var arrDB = this.storageData.fnGetDB() ? this.storageData.fnGetDB() : this.arrDB;
        var delIndex = arrDB.map(function (obj) {
            return obj.id;
        }).indexOf(parseInt(delId));
        if (delIndex > -1) {
            arrDB.splice(delIndex, 1);
            this.storageData.fnSetDB(arrDB);
        }
        /*var arrDB = this.storageData.fnGetDB() ? this.storageData.fnGetDB() : this.arrDB;
        arrDB.splice(delId, 1);
        this.storageData.fnSetDB(arrDB);*/
    };

    ServiceApi.prototype.search = function (searchid) {
        var arrDB = this.storageData.fnGetDB() ? this.storageData.fnGetDB() : this.arrDB;
        if (searchid !== '') {
            return  arrDB.filter(function (obj) {
                return obj.label === searchid || obj.value === searchid || obj.id === searchid;
            });
        } else {
            return this.storageData.fnGetDB();
        }
    };

    return ServiceApi;
})();




