var StorageData = (function () {

    function StorageData(arrDB) {
        if (!localStorage.db) {
            this.fnSetDB(arrDB);
        }
    }
    StorageData.prototype.fnGetDB = function () {
        return localStorage.db ? this.fnDecode(localStorage.db) : [];
    };
    StorageData.prototype.fnSetDB = function (arrDB) {
        localStorage.db = this.fnEncode(arrDB);
    };
    StorageData.prototype.fnEncode = function (arrDB) {
        return window.btoa(JSON.stringify(arrDB));
    };

    StorageData.prototype.fnDecode = function (arrDB) {
        return JSON.parse(window.atob(arrDB));
    };

    return StorageData ;
})();