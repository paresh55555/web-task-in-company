var Controller = (function () {

    function Controller() {
        //create constructor
        this.inputTypeEle = document.getElementById("inputType");
        this.inputTypeOptionEle = document.getElementById("inputTypeOption");
        this.apiCall = new ServiceApi();
    }
    Controller.prototype.validation = function () {
        if (this.inputTypeEle.value === "") {
            alert("Please enter field name.");
            return false;
        } else if (this.inputTypeOptionEle.value === "") {
            alert("Please select input type.");
            return false;
        } else {
            return true;
        }
    };
    Controller.prototype.addDataList = function () {
        if(this.validation()){
            var inputDataObj = {};
            inputDataObj.id = Date.now();
            inputDataObj.label = this.inputTypeEle.value;
            inputDataObj.inputType = this.inputTypeOptionEle.value;
            inputDataObj.isEdit = false;
            inputDataObj.value = "";
            inputDataObj.action = "";
            this.apiCall.addApi(inputDataObj);
            this.generateDynamicTable();
        }
    };

    Controller.prototype.generateDynamicTable = function (searchData) {
        var self = this;
        /*var  inputFilter = document.getElementById("myInput").value;
        console.log(inputFilter);*/
        var allGetData = this.apiCall.getData();
        var data = searchData ? searchData : allGetData;
       /* console.log('Tabledata..' ,data);*/
        var tableDataLength = data.length;
        if (tableDataLength > -1) {
            // CREATE DYNAMIC TABLE.
            var table = document.createElement("table");
            table.setAttribute("class", "table table-bordered table-hover table-striped");
            // retrieve column header ("id", "label","value" and "action")
            // hiding the two column ("isEdit" and "inputType)
            var col = []; // define an empty array
            for (var i = 0; i < tableDataLength; i++) {
                for (var key in data[i]) {
                    if (col.indexOf(key) === -1) {
                        if (key !== "isEdit" && key !== "inputType") {
                            col.push(key);
                        }
                    }
                }
            }
            // CREATE TABLE HEAD .
            var tHead = document.createElement("thead");
            // CREATE ROW FOR TABLE HEAD .
            var hRow = document.createElement("tr");
            // ADD COLUMN HEADER TO ROW OF TABLE HEAD.
            for (var i = 0; i < col.length; i++) {
                var th = document.createElement("th");
                th.setAttribute("class", "text-capitalize");
                th.innerHTML = col[i];
                hRow.appendChild(th);
            }
            tHead.appendChild(hRow);
            table.appendChild(tHead);

            // CREATE TABLE BODY .
            var tBody = document.createElement("tbody");

            // ADD COLUMN HEADER TO ROW OF TABLE HEAD.
            for (var i = 0; i < tableDataLength; i++) {
                var bRow = document.createElement("tr"); // CREATE ROW FOR EACH RECORD .
                for (var j = 0; j < col.length; j++) {
                    var td = document.createElement("td");
                    if (col[j] === "value") {
                        if (data[i].isEdit) {
                            var isEditInputBox = document.createElement("input");
                            isEditInputBox.setAttribute("id", "input" + i);
                            isEditInputBox.setAttribute("value", data[i].value);
                            isEditInputBox.setAttribute("class", "form-control");
                            isEditInputBox.setAttribute("type", data[i].inputType);
                            td.innerHTML = '';
                            td.appendChild(isEditInputBox);
                        } else {
                            td.innerHTML = data[i][col[j]];
                        }
                    } else {
                        td.innerHTML = data[i][col[j]];
                    }

                    if (col[j] === "action") {
                        if (data[i].isEdit) {
                            var updateBtnEle = document.createElement("button");
                            updateBtnEle.setAttribute("id", "update-btn");
                            updateBtnEle.setAttribute("data-id", data[i].id);
                            updateBtnEle.setAttribute("type", "button");
                            updateBtnEle.setAttribute("class", "btn btn-warning");
                            updateBtnEle.setAttribute("update-index", i);
                            updateBtnEle.innerHTML = "Update";
                            updateBtnEle.onclick = function (e) {
                                var updateIndex = e.target.getAttribute("update-index");
                                var  value = document.getElementById("input"+updateIndex).value;
                                var myInput = document.getElementById("myInput");
                                myInput.value = myInput.defaultValue;
                                if (self.inputTypeOptionEle.value === "email") {
                                    var mailFormat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                                    if (value.match(mailFormat)) {
                                        self.apiCall.updRowField(e.target.getAttribute("data-id"), value, false);
                                        self.generateDynamicTable();
                                    } else {
                                        alert('Invalid email Address !!!');
                                        return false;
                                    }
                                }
                                if (self.inputTypeOptionEle.value === "url") {
                                    var urlFormat = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
                                    if (value.match(urlFormat)) {
                                        self.apiCall.updRowField(e.target.getAttribute("data-id"), value, false);
                                        self.generateDynamicTable();
                                    } else {
                                        alert('Invalid web Address !!!');
                                        return false;
                                    }
                                }
                                self.apiCall.updRowField(e.target.getAttribute("data-id"), value, false);
                                self.generateDynamicTable();
                            };
                            td.appendChild(updateBtnEle);
                        } else {
                            var editBtnEle = document.createElement("button");
                            editBtnEle.setAttribute("id", "edit-btn");
                            editBtnEle.setAttribute("type", "button");
                            editBtnEle.setAttribute("edt-value", data[i].id);
                            editBtnEle.setAttribute("class", "btn btn-primary");
                            editBtnEle.setAttribute("edit-index", i);
                            editBtnEle.innerText = "Edit";
                            editBtnEle.onclick = function (e) {
                                /*var  inputFilter = document.getElementById("myInput").value;
                               /!* console.log(inputFilter);*!/

                                var editIndex = e.target.getAttribute("edit-index");*/
                                var edt_value = e.target.getAttribute("edt-value");
                                self.apiCall.edtRowField(edt_value,true);
                                /*self.apiCall.edtRowField(parseInt(editIndex));
                                if(edt_value !== editIndex){
                                    data[editIndex].isEdit = true;
                                    self.generateDynamicTable(self.apiCall.search(edt_value));
                                }
                                self.apiCall.search(edtvalue);
                               if(edt_value){
                                console.log('if part..',edt_value);
                                   self.apiCall.search(inputFilter);
                                   self.generateDynamicTable();
                               }else{
                                   console.log('else part..',edt_value);
                                   self.apiCall.edtRowField(editIndex);
                                   self.generateDynamicTable();
                               }

                                console.log('editttt..',self.apiCall.search(inputFilter));*/

                                self.generateDynamicTable();
                        };
                            td.appendChild(editBtnEle);
                        }
                        if (!data[i].isEdit) {
                            var deleteBtnEle = document.createElement("button");
                            deleteBtnEle.setAttribute("delete-index", i);
                            deleteBtnEle.setAttribute("del-value", data[i].id);
                            deleteBtnEle.setAttribute("class", "btn btn-danger");
                            deleteBtnEle.innerText = "Delete";
                            deleteBtnEle.onclick = function (e) {
                                var del_value = e.target.getAttribute("del-value");
                                var myInput = document.getElementById("myInput");
                                myInput.value = myInput.defaultValue;
                                var confirmBox = confirm("Are you sure want to delete this record?");
                                if (confirmBox) {
                                    self.apiCall.delRowField(del_value);
                                    self.generateDynamicTable();
                                } else {
                                    return true;
                                }
                            };
                            td.appendChild(deleteBtnEle);
                        }
                    }
                    bRow.appendChild(td);
                }
                tBody.appendChild(bRow)
            }
            table.appendChild(tBody);
            var divContainer = document.getElementById("table-list");
            divContainer.innerHTML = "";
            divContainer.appendChild(table);
        }
    };

    Controller.prototype.filterSearch = function () {
        var  inputFilter = document.getElementById("myInput").value;
       /* console.log(inputFilter);*/
        this.generateDynamicTable(this.apiCall.search(inputFilter));
    };

    Controller.prototype.download_csv = function(csv, filename){
        var csvFile;
        var downloadLink;
        csvFile = new Blob([csv], {type: "text/csv"});
        downloadLink = document.createElement("a");
        downloadLink.download = filename;
        downloadLink.href = window.URL.createObjectURL(csvFile);
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
        downloadLink.click();
    };

    Controller.prototype.exportTableToCSV = function(filename){
        var csv = [];
        var rows = document.querySelectorAll("#table-list tr");
        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");
            for (var j = 0; j < cols.length; j++)
                row.push(cols[j].innerText);
                csv.push(row.join(","));
        }
        // Download CSV
        this.download_csv(csv.join("\n"), filename);
    };
    return Controller;
})();

