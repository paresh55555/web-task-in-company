
var routingmodule = angular.module("routing_app", ["ngRoute"]);
routingmodule.config(function($routeProvider) {
    $routeProvider
        .when("", {
            templateUrl : "main2.html"
        })
        .when("/page1", {
            templateUrl : "page1.html"
        })
        .when("/page2", {
            templateUrl : "page2.html"
        })
        .when("/page3", {
            templateUrl : "page3.html"
        });
});
routingmodule.controller('sorted_data',function ($scope) {
    $scope.arraylist = [
        {name:'Mumbai',country:'India'},
        {name:'Delhi',country:'India'},
        {name:'Bangalore',country:'India'},
        {name:'Hyderabad',country:'India'},
        {name:'Ahmedabad',country:'India'},
        {name:'Chennai',country:'India'},
        {name:'Kolkata',country:'India'},
        {name:'Washington',country:'US'},
        {name:'Greenville',country:'US'}
    ];
    $scope.orderbydata = function(x){
        $scope.getOrderlist = x;
    }
});
routingmodule.controller('radio_ctrl',function($scope){
    console.log('Data output....');
});

routingmodule.controller('dropdown-ctrl',function ($scope) {

});

