
    var optionmenumodule = angular.module('option_drp',[]);
    optionmenumodule.controller('dropdown_array', function ($scope) {
        $scope.dataArryDynamic =['select city','Mumbai','Delhi','Bangalore','Hyderabad','Ahmedabad','Chennai','Kolkata','Surat','Jaipur'];
        $scope.selectedName = "select city";
    });

    var httpdata  = angular.module('http_service',[]);
    httpdata.controller('service_get',function ($scope,$http) {
        $http.get("demo.txt").then(function(response) {
            $scope.mydatapage = response.data;
        });
    });

    var sortedTable = angular.module('sorted_filter',[]);
    sortedTable.controller('sorted_data',function ($scope) {
        $scope.arraylist = [
            {name:'Mumbai',country:'India'},
            {name:'Delhi',country:'India'},
            {name:'Bangalore',country:'India'},
            {name:'Hyderabad',country:'India'},
            {name:'Ahmedabad',country:'India'},
            {name:'Chennai',country:'India'},
            {name:'Kolkata',country:'India'},
            {name:'Washington',country:'US'},
            {name:'Greenville',country:'US'}
        ];
        $scope.orderbydata = function(x){
            $scope.getOrderlist = x;
        }
    });

    var app = angular.module('myApp', []);
    app.controller('myCtrl', function($scope) {
        $scope.count = 0;
    });

    var counter = angular.module('counter_app',[]);
    counter.controller('counter_ctrl',function ($scope) {
       $scope.countVal = 0;
       $scope.countfunc = function () {
           $scope.countVal++;
       }
    });
    var showMenu = angular.module('show_menu_app',[]);
    showMenu.controller('show_menu_data',function ($scope) {
        $scope.getmenu =function () {
            $scope.data_menu = ! $scope.data_menu;
        }  
    });

    var mousemoveapp = angular.module('mousemove',[]);
    mousemoveapp.controller('mouse_ctrl',function ($scope) {
       $scope.coordinate = function (position) {
           $scope.x = position.clientX;
           $scope.y = position.clientY;
       } 
    });

    var radiobuttonapp = angular.module('radio_app',[]);
    radiobuttonapp.controller('radio_ctrl',function($scope){
            console.log('Data output....');
    });

    var drpmodule = angular.module('drp-app',[]);

    drpmodule.controller('dropdown-ctrl',function ($scope) {
        
    });

    var formapp = angular.module('data_form',[]);
    formapp.controller('data_form_ctrl',function ($scope) {
        $scope.userobj = {firstname: ' ',lastname:' '};
        $scope.userfunc  = function () {
            $scope.userval = angular.copy($scope.userobj);
        }
    });






    angular.element(function() {
        angular.bootstrap(document,
            ['option_drp',
             'http_service',
             'sorted_filter',
             'myApp',
             'counter_app',
             'show_menu_app',
             'mousemove',
             'radio_app',
             'drp-app',
             'data_form'

            ]);
    });