var sortedTable = angular.module('sorted_filter',[]);
sortedTable.controller('sorted_data',function ($scope) {
    $scope.arraylist = [
        {name:'Mumbai',country:'India'},
        {name:'Delhi',country:'India'},
        {name:'Bangalore',country:'India'},
        {name:'Hyderabad',country:'India'},
        {name:'Ahmedabad',country:'India'},
        {name:'Chennai',country:'India'},
        {name:'Kolkata',country:'India'},
        {name:'Washington',country:'US'},
        {name:'Greenville',country:'US'}
    ];
    $scope.orderbydata = function(x){
        $scope.getOrderlist = x;
    }
});