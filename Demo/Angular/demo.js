var app = angular.module('myapp',[])
app.controller('myctr',function($scope){
    $scope.qyt = 'Select Color';
    $scope.cst = 'Select Color';
    $scope.clr = 'Select Color';
    $scope.textmsg = '';

    $scope.myFunc = function() {
        $scope.result = $scope.qyt * $scope.cst ;

    };
    $scope.text = function(){
        $scope.msg = $scope.textmsg;

        console.log( $scope.msg)
    }
});

var app2 = angular.module('myapp2',[]);
app2.controller('form_ctrl',function($scope){

    $scope.objdata = {f_name:"",l_name:""};
    $scope.dataget = function () {
        $scope.information = angular.copy($scope.objdata);
    };
    $scope.addobj = function () {
        $scope.addobjdata = angular.copy({f_name:"paresh",l_name:"vaghasiya"});
    };
    $scope.dataget();
});

var filterModule = angular.module('filterapp',[]);
filterModule.controller('filter-ctrl',function ($scope) {
    $scope.arrayData = ['Mumbai','Delhi','Bangalore','Hyderabad','Ahmedabad','Chennai','Kolkata','Surat','Jaipur']


});

var sortedTable = angular.module('sorted_filter',[]);
sortedTable.controller('sorted_data',function ($scope) {
    $scope.arraylist = [
        {name:'Mumbai',country:'India'},
        {name:'Delhi',country:'India'},
        {name:'Bangalore',country:'India'},
        {name:'Hyderabad',country:'India'},
        {name:'Ahmedabad',country:'India'},
        {name:'Chennai',country:'India'},
        {name:'Kolkata',country:'India'},
        {name:'Washington',country:'US'},
        {name:'Greenville',country:'US'}
    ];
    $scope.orderbydata = function(x){
        $scope.getOrderlist = x;
    }
});

var customFilter = angular.module('filter_custom',[]);
customFilter.controller('custom_filter_ctrl',function ($scope) {
     $scope.arraycustom = ['Mumbai','Delhi','Bangalore','Hyderabad','Ahmedabad','Chennai','Kolkata','Surat','Jaipur'];

});

customFilter.filter('mycustomformate', function() {
    return function(x) {
        var i, c, txt = "";
        for (i = 0; i < x.length; i++) {
            c = x[i];
            /*if (i % 2 == 0) {
             /!* c = c.toUpperCase();*!/
             c = c.toLowerCase();
             }*/
            c = c.toLowerCase();
            txt += c;
        }
        return txt;
    };
});


var urllink = angular.module('abturl',[]);
urllink.controller('url_ctrl',function ($scope ,$location) {
    $scope.geturllink = function () {
        $scope.geturl = $location.absUrl();
    }

});


var timerset = angular.module('statusText',[]);
timerset.controller('Text_change',function ($scope,$timeout) {
    $scope.mytext = 'Heloo...';
    $timeout(function () {
       $scope.mytext = 'how r you???'
    },500);

});

    var httpdata  = angular.module('http_service',[]);
    httpdata.controller('service_get',function ($scope,$http) {
        $http.get("demo.txt").then(function(response) {
            $scope.mydatapage = response.statusText;
        });
    });


    var jsonmodule = angular.module('dataJson',[]);
    jsonmodule.controller('json_file',function($scope,$http){
        $http.get('objectdata.json').then(function (response) {
            $scope.myjsondata = response.data.records;

            $scope.msgdata = function () {
                $scope.myjsondata = response.data.records;
                alert($scope.myjsondata)
            }

        })
    });


    var tableData = angular.module('tabledata',[]);
    tableData.controller('table_ctrl',function($scope,$http){
        $http.get('objectdata.json').then(function (respons) {
            $scope.gettable = function () {
                $scope.gettabledata = respons.data.records;
            };
            $scope.orderdata  = function(y){
                if($scope.sortedData == y) {
                    $scope.reverse=!$scope.reverse;
                }
                $scope.sortedData = y;
            }
        });
    });

    var optionmenumodule = angular.module('option_drp',[]);
    optionmenumodule.controller('dropdown_array', function ($scope) {

        $scope.dataArryDynamic =['select city','Mumbai','Delhi','Bangalore','Hyderabad','Ahmedabad','Chennai','Kolkata','Surat','Jaipur'];
        $scope.selectedName = "select city";
    });

angular.element(function() {

    angular.bootstrap(document, ['myapp',
        'myapp2',

        'filterapp',
        'sorted_filter',
        'filter_custom',
        'abturl',
        'statusText',
        'http_service',
        'dataJson',
        'tabledata',
        'option_drp'

    ]);
});