
var tableModule = angular.module('table_App',[]);
tableModule.controller('table_ctrl',function ($scope) {

        $scope.myArray = [
            { "CustomerName": "FORD", "Australia": 0, "China": 2, "India": 0, "South Korea": 0 },
            { "CustomerName": "ICICI PRUDENTIAL", "Australia": 0, "China": 0, "India": 5, "South Korea": 0 },
            { "CustomerName": "Kimberly Clark", "Australia": 0, "China": 0, "India": 0, "South Korea": 1 },
            { "CustomerName": "McDonalds", "Australia": 1, "China": 0, "India": 0, "South Korea": 0 },
            { "CustomerName": "Novartis", "Australia": 1, "China": 0, "India": 0, "South Korea": 0 },
            { "CustomerName": "Origin Energy", "Australia": 3, "China": 0, "India": 0, "South Korea": 0 }
        ];
    $scope.removedata = function (x) {

        $scope.myArray.splice(x , 1);
    }
});