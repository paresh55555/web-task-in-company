require.config({
    paths: {
        "underscore": "vendor/underscore/underscore",
        "backbone": "vendor/backbone/backbone",
        "jquery": "vendor/jquery/dist/jquery",
        "backbone.babysitter": "vendor/backbone.babysitter/lib/backbone.babysitter",
        "backbone.wreqr": "vendor/backbone.wreqr/lib/backbone.wreqr",
        "modernizr": "vendor/modernizr/modernizr",
        "semantic": "vendor/semantic/dist/semantic",
        "text": "vendor/requirejs-text/text",
        "marionette": "vendor/backbone.marionette/lib/backbone.marionette",
        "requirejs": "vendor/requirejs/require",
        "main": "main"
    },
    shim: {
        jquery: {
            exports: "$"
        },
        underscore: {
            exports: "_"
        },
        backbone: {
            deps: [
                "underscore",
                "jquery"
            ],
            exports: "Backbone"
        },
        babysitter: {
            deps: [
                "backbone"
            ]
        },
        wreqr: {
            deps: [
                "backbone"
            ]
        },
        marionette: {
            deps: [
                "backbone.wreqr",
                "backbone.babysitter"
            ],
            exports: "Marionette"
        },
        modernizr: {
            exports: "Modernizr"
        },
        semantic: {
            exports: "Semantic"
        },
        text: {
            exports: "Text"
        },
        main: {
            deps: [
                "marionette",
                "backbone"
            ]
        }
    },
    packages: [

    ]
});

define([
    "jquery",
    "underscore",
    "backbone",
    "marionette",
    "modernizr"
], function () {
    "use strict";
    define(["main"], function (App) {
        App.start();
    });
});
