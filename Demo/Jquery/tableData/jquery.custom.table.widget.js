/**
 * Created by - on 4/9/2014.
 */
$.widget("custom.table",{
    options: {
        readData:null,
        pagination:false,
        sort:false,
        filter:false,
        columnWiseFilter:false,
        filterElementColor:"green",
        numPerPage:10
    },
    _init:function()
    {
        var self=this;
        //apply padding to widget
        $(self.element).css("padding","30px");

        //create drop down
        this._dropDownNumOfItems=$("<select style='float: left'></select>").appendTo(this.element);

        //create search filter
        this._searchFilter=$("<input type='text' style='float: right;padding-bottom:1px' placeholder='Search......'/>").appendTo(this.element);

        //create drop down
        this._columnWiseFilterDropDown=$("<select style='float: right'></select>").appendTo(this.element);

        //create table
        this.table = $("<table></table>").appendTo(this.element);
        this.tableHeader = $("<thead></thead>").appendTo(this.table);
        this.tableBody = $("<tbody></tbody>").appendTo(this.table);
        this.tableRow = $("<tr></tr>").appendTo(this.tableHeader);

        //create view for record
        this.view = $("<div style='float: left; margin: 0.5em 0;'></div>").appendTo(this.element);

        //create text box for enter page no.
        this.pageNoText=$("<input type='text' maxlength='3' id='pageNumber'style='display:none; float: left; margin: 0.5em; max-width: 25px;margin-left: 10px;border: 1px solid'/>").appendTo(this.element);

        //create pager
        this.pager = $('<div class="pager" style="float: right"></div>').appendTo(this.element);

        //create table header
        var jsonData = this.options.readData
        var arr = Object.keys(this.options.readData[0])
        for(var intIndex=0;intIndex<arr.length;intIndex++) {
            if(this.options.sort == true)
            {
                //if sorting true
                this.tableHeaderCol = $("<th id="+arr[intIndex]+"><a href=' javascript:void(0)'>" +arr[intIndex]+"</a></th>").appendTo(this.tableRow);
            }
            else
            {
                this.tableHeaderCol = $("<th>"+arr[intIndex]+"</th>").appendTo(this.tableRow);
            }
        }

        //create table data
        for(var intIndex=0;intIndex<jsonData.length;intIndex++)
        {
            this.tableRow = $("<tr></tr>").appendTo(this.tableBody);
            for(var i=0;i<arr.length;i++) {
                var colName=arr[i];
                $("<td>" + jsonData[intIndex][colName] +"</td>").appendTo(this.tableRow);
            }
        }

        //if pagination true
        if(this.options.pagination == true){
            var option = [3,5,10,20,50,100];
            var flag=0;
            for(var intIndex=0;intIndex<option.length;intIndex++)
            {
                if(option[intIndex]==this.options.numPerPage)
                {
                    //created option select
                    $("<option selected value="+option[intIndex]+">"+option[intIndex]+"</option>").appendTo(this._dropDownNumOfItems);
                    this.pagination(option[intIndex],self,this.pager,this.view);
                    flag=1;
                    continue;
                }
                else{
                    if(flag==0)
                    {
                        if(option[intIndex]==10) // if user set wrong value of numPerPage option or property
                        {
                            //created option select
                            $("<option selected value="+option[intIndex]+">"+option[intIndex]+"</option>").appendTo(this._dropDownNumOfItems);
                            this.pagination(option[intIndex],self,this.pager,this.view);
                            flag=1;
                            continue;
                        }
                    }
                    //create option
                    $("<option value="+option[intIndex]+" >"+option[intIndex]+"</option>").appendTo(this._dropDownNumOfItems);
                }
            }
            //on drop-down change pagination and view num record to be change
            this._dropDownNumOfItems.change(function () {
                self.pager.empty();
                self.pagination(this.value,self,self.pager,self.view);
                self.view.text('View '+1+' - '+this.value+' of '+self.options.readData.length);
            });
            self.view.text('View '+1+' - '+this._dropDownNumOfItems.val()+' of '+this.options.readData.length);
            self.pageNoText.keypress(function(e) {
                if(e.which == 13) {
                    console.log('You pressed enter!'+this.value);
                    var startIndex=0;
                    var lastIndex=15;

                    for(var intIndex=startIndex;intIndex<lastIndex;intIndex++)
                    {
                        if(intIndex == this.value)
                        {
                            console.log("found");
                        }
                        else
                        {
                            startIndex = lastIndex + 1;
                            lastIndex = lastIndex +15;
                        }
                    }
//                    $("#page"+this.value).show(1);
//                    $(".active").hide(1);
                }
            });
        }else{
            //if pagination is false then drop down is hide
            this._dropDownNumOfItems.hide();
        }

        //if search filter is true
        if(this.options.filter==true){
            this._searchFilter.keyup(function () {
                //console.log(self._searchFilter.val())
                self.searchTable(self._searchFilter.val(),self,self._dropDownNumOfItems.val());
            });
        }
        else{
            //if filter is false then search text box is hide
            this._searchFilter.hide();
        }

        //if column wise search filter true
        if(this.options.columnWiseFilter == true)
        {
            if(this._searchFilter.hide())
            {
                this._searchFilter.show();
            }
            //get column name
            var arr = Object.keys(this.options.readData[0])
            if(this.options.filter==true)
            {
                //if both filter are true then it's create and work with 1st filter
                $("<option value='none'>Select Column</option>").appendTo(this._columnWiseFilterDropDown);
            }
            for(var intIndex=0;intIndex<arr.length;intIndex++) {
                $("<option value="+intIndex+">"+arr[intIndex]+"</option>").appendTo(this._columnWiseFilterDropDown);
            }
            this._searchFilter.keyup(function () {
                if(self.options.filter==true && self._columnWiseFilterDropDown.val()=='none')//if both filter are true then 1st filter is true then it work with select column drop down
                {
                    self.searchTable(self._searchFilter.val(),self,self._dropDownNumOfItems.val());
                }
                else
                {
                    //console.log(self._columnWiseFilterDropDown.val())
                    self.columnWiseSearchTable(self._searchFilter.val(),self,self._dropDownNumOfItems.val(),self._columnWiseFilterDropDown.val());
                }
            });
        }
        else
        {
            this._columnWiseFilterDropDown.hide();
        }
        //if sorting is true
        if(this.options.sort == true){
            //get table header id to apply sorting on each column
            var arr = Object.keys(this.options.readData[0])
            for(var intIndex=0;intIndex<arr.length;intIndex++) {
                arr[intIndex] = "#" + arr[intIndex];
            }
            var headerId = arr.join();
            $(headerId,self.element).wrapInner('<span title="Sort This Column">').each(function(){
                var th = $(this),
                    thIndex = th.index(),
                    inverse = false;
                th.click(function(){
                    $('table',self.element).find('td').filter(function(){
                        return $(this).index() === thIndex;
                    }).sortElement(function(a, b){
                        //console.log($.text([a])+" "+$.text([b]));
                        return $.text([a]) > $.text([b]) ?
                            inverse ? -1 : 1
                            : inverse ? 1 : -1;
                    }, function(){
                        // parentNode is the element we want to move
                        return this.parentNode;
                    });
                    inverse = !inverse;
                });
            });
            jQuery.fn.sortElement = (function(){
                var sort = [].sort;
                return function(comparator, getSortable) {
                    getSortable = getSortable || function(){return this;};
                    var placements = this.map(function(){
                        var sortElement = getSortable.call(this),
                            parentNode = sortElement.parentNode,
                            nextSibling = parentNode.insertBefore(
                                document.createTextNode(''),
                                sortElement.nextSibling
                            );
                        return function() {
                            if (parentNode === this) {
                                throw new Error(
                                    "You can't sort elements if any one is a descendant of another."
                                );
                            }
                            // Insert before flag:
                            parentNode.insertBefore(this, nextSibling);
                            // Remove flag:
                            parentNode.removeChild(nextSibling);
                        };
                    });
                    return sort.call(this, comparator).each(function(i){
                        placements[i].call(getSortable.call(this));
                    });
                };
            })();
        }
    },
    pagination:function(itemsPerPage,_this,_pager,_view)
    {
        var $increment = 15;
        $(_this.table).each(function() {
            var currentPage = 0;
            var numPerPage = itemsPerPage;
            var $table = $(this);
            $table.bind('repaginate', function() {
                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            });
            $table.trigger('repaginate');
            var numRows = $table.find('tbody tr').length;
            var numPages = Math.ceil(numRows / numPerPage);
            var $pager = _pager;
            var $previous = $('<span class="previous"><<</spnan>').appendTo($pager);
            var $first = $('<span class="page-number"></span>');
            var $last = $('<span class="page-number"></span>');
            var $view = _view;
            var flag=0;//for create last page no.
            var boolean=0;//for next no. of page
            for (var page = 0; page < numPages; page++) {
                if(page==0)
                {
                    $first.text('First').bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');

                        //view number of records
                        var pageNo=currentPage+1;
                        var lastRowNo=pageNo*itemsPerPage;
                        var firstRowNo=lastRowNo-itemsPerPage+1;
                        $view.empty();
                        $view.text('View '+firstRowNo+' - '+lastRowNo+' of '+_this.options.readData.length);

                        if(boolean==1){
                            //when click on first then show 1st 15 page no.
                            if($(".active",_this.element).text()== "First" && $increment >= 15 ){
                                $increment = 15;
                                var $startIndex = 0;
                                for(var intIndex=$startIndex;intIndex<=$increment;intIndex++){
                                    $("#page"+intIndex,_this.element).show(1000);
                                }
                                for(var intIndex=$increment+1;intIndex<=numPages;intIndex++){
                                    $("#page"+intIndex,_this.element).hide(1);
                                }
                            }
                        }
                    }).appendTo($pager).addClass('clickable');
                }
                else if(page==numPages-1){//create last page
                    flag=1;
                }
                else{
                    var $pageNoSpan=$('<span class="page-number"></span>').attr("id","page"+(page+1));
                    $pageNoSpan.text(page + 1).bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');

                        //view number of records
                        var pageNo=currentPage+1;
                        var lastRowNo=pageNo*itemsPerPage;
                        var firstRowNo=lastRowNo-itemsPerPage+1;
                        $view.empty();
                        $view.text('View '+firstRowNo+' - '+lastRowNo+' of '+_this.options.readData.length);
                    }).appendTo($pager).addClass('clickable');
                    if(page>15-1)
                    {
                        //if page create more then 15 then it will hide
                        $pageNoSpan.hide();
                        boolean=1;
                    }
                }
                if(flag==1)
                {
                    $last.text('Last').bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');

                        //view number of records
                        var pageNo=numPages;
                        var lastRowNo=pageNo*itemsPerPage;
                        var firstRowNo=lastRowNo-itemsPerPage+1;
                        $view.empty();
                        $view.text('View '+firstRowNo+' - '+_this.options.readData.length+' of '+_this.options.readData.length);

                        if(boolean==1){
                            //when click on first then show last no of page.
                            if($(".active",_this.element).text()== "Last" && numPages >(numPages % 15)){
                                var $inc = numPages % 15;
                                if($inc==0)
                                {
                                    $inc = numPages-15;
                                    for(var intIndex=0;intIndex<=$inc;intIndex++){
                                        $("#page"+intIndex,_this.element).hide(1);
                                    }
                                    for(var intIndex=numPages;intIndex>$inc;intIndex--){
                                        $("#page"+intIndex,_this.element).show(1000);
                                    }
                                }else
                                {
                                    for(var intIndex=0;intIndex<=numPages-$inc;intIndex++){
                                        $("#page"+intIndex,_this.element).hide(1);
                                    }
                                    for(var intIndex=numPages;intIndex>numPages-$inc;intIndex--){
                                        $("#page"+intIndex,_this.element).show(1000);
                                    }
                                }
                            }
                        }
                    }).appendTo($pager).addClass('clickable');
                }
            }
            $pager.find('span.page-number:first').addClass('active');
            var $next = $('<span class="next">>></spnan>').appendTo($pager);
            $next.click(function (e) {
                $previous.addClass('clickable');
                $pager.find('.active').next('.page-number.clickable').click();

                if(boolean==1){
                    //when click on next then show next 15 page no.
                    if($increment == $(".active",_this.element).text()-1){
                        var $startIndex = 0;
                        var $lastIndex = "";
                        for(var intIndex=$startIndex;intIndex<=$increment;intIndex++){
                            $("#page"+intIndex,_this.element).hide(1);
                            $lastIndex++;
                        }
                        $increment = $increment + 15;
                        for(var intIndex=$lastIndex;intIndex<=$increment;intIndex++){
                            $("#page"+intIndex,_this.element).show(1000);
                        }
                    }
                }
            });
            $previous.click(function (e) {
                $next.addClass('clickable');
                $pager.find('.active').prev('.page-number.clickable').click();

                if(boolean==1){
                    //when click on previous then show previous 15 page
                    var numArr = [];
                    for(var intIndex=0;intIndex<numPages;intIndex++)
                    {
                        numArr[intIndex] = 15 * intIndex;
                        if(numArr[intIndex] == ($(".active",_this.element).text())){
                            var $dec = numArr[intIndex]-15;
                            if(numArr[intIndex] == 15)
                            {
                                for(var intIndex=numArr[intIndex];intIndex>=2;intIndex--){
                                    $("#page"+intIndex,_this.element).show(1000);
                                }
                                for(var intIndex=numArr[intIndex]+1;intIndex<=30;intIndex++){
                                    $("#page"+intIndex,_this.element).hide(1);
                                }
                            }
                            else if(numArr[intIndex] != 15)
                            {
                                var $inc = numArr[intIndex] + 15
                                for(var intIndex=numArr[intIndex];intIndex>$dec;intIndex--){
                                    $("#page"+intIndex,_this.element).show(1000);
                                }
                                for(var intIndex=($inc-15)+1;intIndex<=$inc;intIndex++){
                                    $("#page"+intIndex,_this.element).hide(1);
                                }
                            }
                        }
                    }
                    if($(".active",_this.element).text() != "First")
                        $increment = parseInt($(".active",_this.element).text());
                }
            });
            $table.on('repaginate', function () {
                $next.addClass('clickable');
                $previous.addClass('clickable');
                setTimeout(function () {
                    var $active = $pager.find('.page-number.active');
                    if ($active.next('.page-number.clickable').length === 0) {
                        $next.removeClass('clickable');
                    } else if ($active.prev('.page-number.clickable').length === 0) {
                        $previous.removeClass('clickable');
                    }
                });
            });
            $table.trigger('repaginate');
        });
    },
    searchTable:function(inputVal,_this,_numPerPage)
    {
        var currentPage=0;
        var numPerPage=_numPerPage;
        //console.log(numPerPage)
        var table = $('table',_this.element);
        table.find('tr').each(function(index, row)
        {
            var allCells = $(row).find('td');
            if(allCells.length > 0)
            {
                var found = false;
                allCells.each(function(index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if(inputVal=="")
                    {
                        $(".coloredElement",_this.element).css("color","#000000");
                    }
                    else
                    {
                        $(this,_this.element).html($(this).text().replace(regExp, "<span style='color: "+_this.options.filterElementColor+"' class='coloredElement'>"+inputVal+"</span>"));
                    }
                    if(regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });
                if(found == true)$(row).show();else $(row).hide();
            }
        });
        //if search text box is empty then view only num of row that set by user or default
        if(numPerPage != null)
        {
            if(inputVal=="")
            {
                table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            }
        }
    },
    columnWiseSearchTable:function(inputVal,_this,_numPerPage,_column)
    {
        var currentPage=0;
        var numPerPage=_numPerPage;
        //console.log(numPerPage)
        var table = $('table',_this.element);
        table.find('tr').each(function(index, row)
        {
            var allCells = $(row).find('td').eq(_column);
            if(allCells.length > 0)
            {
                var found = false;
                allCells.each(function(index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if(inputVal=="")
                    {
                        $(".coloredElement",_this.element).css("color","#000000");
                    }
                    else
                    {
                        $(this,_this.element).html($(this).text().replace(regExp, "<span style='color: "+_this.options.filterElementColor+"' class='coloredElement'>"+inputVal+"</span>"));
                    }
                    if(regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });
                if(found == true)$(row).show();else $(row).hide();
            }
        });
        //if search text box is empty then view only num of row that set by user or default
        if(numPerPage != null)
        {
            if(inputVal=="")
            {
                table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            }
        }
    }
});
