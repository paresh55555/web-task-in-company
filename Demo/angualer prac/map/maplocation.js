var module = angular.module("angular-google-maps-example", ["google-maps"]);
function ExampleController ($scope) {
    angular.extend($scope, {
        centerProperty: {
            lat: -4.930217,
            lng: 26.920921
        },
        zoomProperty: 8,
        markersProperty: [ {
            latitude: -4.930217,
            longitude: 26.920921
        }],
        clickedLatitudeProperty: null,
        clickedLongitudeProperty: null,
    });
}