var application = angular.module('search-app',[]);
application.filter('searchFor', function(){
    return function(arr, searchString){
        if(!searchString){
            return arr;
        }
        var result = [];
        searchString = searchString.toLowerCase();

        // Using the forEach helper method to loop through the array
        angular.forEach(arr, function(item){
            if(item.title.toLowerCase().indexOf(searchString) !== -1){
                result.push(item);
            }
        });
        return result;
    };
});
application.controller('search_ctrl',function ($scope) {

    $scope.arrayList = [
        {
            image: 'http://www.tutorialspoint.com/android/images/android-mini-logo.jpg'

        }
    ]


});